﻿using FoodSafetyManagement.DAL;
using FoodSafetyManagement.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FoodSafetyManagement.Controllers
{
    public class DataManagementController : Controller
    {
        FoodSafetyDBDataContext foodSafetyDb = new FoodSafetyDBDataContext();
        // GET: DataManagement
        public ActionResult Index()
        {
            return View();
        }

        // GET: DataManagement/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        [HttpPost]
        // POST: DataManagement/GetAll
        public string GetAll(int pagenumber)
        {
            try
            {

                var response = new APIResponse(HttpStatusCode.OK);
                var data = (from d in foodSafetyDb.FoodSafetyTbls select d).ToList();
                response.ResponseObject = BsonSerializer.Deserialize<List<DataViewModel>>(data.ToJson());
                return response.ToJson();

            }
            catch (Exception ex)
            {
                return new APIResponse(HttpStatusCode.InternalServerError)
                {
                    ResponseMessage = ex.ToString() + "Error"
                }.ToJson();

            }

        }
        [HttpPost]
        // POST: DataManagement/Create
        public DAL.APIResponse Create(FoodSafetyTbl model)
        {
            try
            {
               
                var record = (from f in foodSafetyDb.FoodSafetyTbls where f.ID == model.ID select f).FirstOrDefault();
                
                if (record!=null)
                {
                   var properties = record.GetType().GetProperties();
                   foreach(var prop in properties)
                   {
                        record.GetType().GetProperty(prop.Name).SetValue(record, model.GetType().GetProperty(prop.Name).GetValue(model));
                   }
                    foodSafetyDb.SubmitChanges();
                }
                else
                {
                    foodSafetyDb.FoodSafetyTbls.InsertOnSubmit(model);
                     foodSafetyDb.SubmitChanges();
                }
                var response = new APIResponse(HttpStatusCode.OK);
                response.ResponseMessage = "Record saved successfulyy";
                return response;
            }
            catch (Exception ex)
            {
                return new APIResponse(HttpStatusCode.InternalServerError)
                {
                    ResponseMessage = ex.ToString() + "Error"
                };
               
            }
          
        }
        [HttpPost]
        // POST: DataManagement/GetCustomerByBarCode
        public string GetCustomerByBarCode(string barcode )
        {
            try
            {
                var response = new APIResponse(HttpStatusCode.OK);
                var data = (from d in foodSafetyDb.FoodSafetyTbls
                            where d.BARCODE == barcode
                            select d).FirstOrDefault();
                response.ResponseObject = BsonSerializer.Deserialize<DataViewModel>(data.ToJson());
                response.ResponseMessage = "success";
                return response.ToJson();
            }
            catch (Exception ex)
            {
                return new APIResponse(HttpStatusCode.InternalServerError)
                {
                    ResponseMessage = ex.ToString() + "Error"
                }.ToJson();

            }

        }


        // GET: DataManagement/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: DataManagement/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        { 
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: DataManagement/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: DataManagement/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
