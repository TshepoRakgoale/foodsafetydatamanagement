﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FoodSafetyManagement.DAL
{
   public class APIResponse
    {
        public HttpStatusCode statusCode { get; set; }
        public object ResponseObject { get; set; }
        public string ResponseMessage { get; set; }
        public APIResponse(HttpStatusCode code)
        {
            statusCode = code;
        }
    }
}
