﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodSafetyManagement.Models
{
    public class DataViewModel
    {
        public int? ID{get;set;}

        public int? TOTAL{get;set;}

        public int? D1{get;set;}

        public int? PP2{get;set;}

        public int? OTHER{get;set;}

        public string CUSTOMER{get;set;}

        public string DOSINGPUMP{get;set;}

        public string CONTACT{get;set;}

        public string TELEPHONE{get;set;}

        public string CELL{get;set;}

        public int? RENTALS{get;set;}

        public int? E_BAR{get;set;}

        public int? COMENDA{get;set;}

        public int? DIHR{get;set;}

        public int? CLASSEQ{get;set;}

        public int? HOBART{get;set;}

        public int? COLGED{get;set;}

        public int? ELECTROLUX{get;set;}

        public int? ANGELOPO{get;set;}

        public int? OMNIWASH{get;set;}

        public int? LUXIA{get;set;}

        public int? ZANUSSI{get;set;}

        public int? OTHER_{get;set;}

        public string LOCATION{get;set;}
        public string BARCODE { get; set; }
    }
}
