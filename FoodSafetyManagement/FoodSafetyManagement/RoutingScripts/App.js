﻿/// <reference path="DataService.js" />

var app = angular.module("myApp", ["ngRoute"]);
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "Templates/HtmlPage1.html",
            controller: "page1"
          
        })
    .when("/page2", {
        templateUrl: "Templates/HtmlPage2.html",
        controller: "page2"
    })
});
app.factory('Scopes', function ($rootScope) {
    var mem = {};

    return {
        store: function (key, value) {
            $rootScope.$emit('scope.stored', key);
            mem[key] = value;
        },
        get: function (key) {
            return mem[key];
        }
    };
});
app.controller("page1", function ($scope, $http,Scopes)
{
    Scopes.store('page1', $scope);
    $scope.showAdd = true;
    $scope.addRecord = function () {
         $.blockUI();
        $http({
            method: "POST",
            url: "DataManagement/Create",
            data:$scope.data,
        }).then(function (response) {
            if (response.status===200) {
                $.unblockUI();
                $scope.data = {};
                ShowMessage("record added successfully", 'success');
            } else {
                $.unblockUI();
                ShowMessage("record not added", 'error');
            }
           
        }, function(reason) {
            alert(reason);
        });
    }

    $scope.EditForm = function (data) {
        //$scope.ScrollToTop();
        $scope.showAdd = false;
        $scope.showEdit = true;
        debugger        
            $scope.data = data;
            $scope.$digest();
    };
});
app.controller("page2", function ($scope, $http,Scopes) {
    Scopes.store('page2', $scope);
    $scope.GetAll = function () {
        $.blockUI();
        $http({
            method: "POST",
            url: "DataManagement/GetAll",
            data:{pagenumber:5,entries:1}
        }).then(function (response) {
            debugger
            $scope.data = response.data.ResponseObject._v;
            $.unblockUI();
        }, function(response) {
            $scope.myWelcome = response.statusText;
        }, function(data){ alert('done')});
    }
    $scope.GetAll();
    $scope.openEditForm = function (data) {
        location.assign("#/");
        setTimeout(function () {
            Scopes.get("page1").EditForm(data);

        }, 500);
        
    }
  
});