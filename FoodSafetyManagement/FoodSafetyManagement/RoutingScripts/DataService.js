﻿
var POST = function (url, data, OnError, OnSucess, OnCompelete) {
    $.blockUI();
    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: data,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            if (OnSuccess) {
                OnSuccess(JSON.parse(result.responseText));


            }
        },
        error: function (jqXHR, testStatus, errorThrown) {
            if (OnError) {
                OnError(jqXHR, testStatus, errorThrown);
            }
        },
        complete: function (result) {
            if (OnCompelete) {
                OnCompelete(JSON.parse(result.responseText));
            }
            $.unblockUI();

        }


    });
}
function ShowMessage(message, type) {
    toastr.options = {
        "positionClass": "toast-bottom-right",
        "showDuration": "600"

    }
    if (type == "success") {
        toastr.success(message);
    }
    if (type == "error") {
        toastr.error(message);
    }
    if (type == "info") {
        toastr.info(message);
    }
    if (type == "warning") {
        toastr.warning(message);
    }
}
    
       
     
     



