﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FoodSafetyManagement.Startup))]
namespace FoodSafetyManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
